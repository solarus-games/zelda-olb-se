-- Lua script of map houses/monster_house.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

function monster_guy:on_interaction()
  local encyclopedia = game:get_item("monsters_encyclopedia")

  if encyclopedia:get_variant() == 0 then
    -- First time.
    encyclopedia:set_variant(1)
    game:start_dialog("houses.monster_house.welcome")
  end

  -- TODO
end
