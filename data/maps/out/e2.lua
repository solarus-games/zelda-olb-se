local map = ...
local game = map:get_game()

function map:on_started()
  if game:get_value("mine_opened") then
    mine_keeper:set_position(mine_keeper_target:get_position())
  end
end

function bad_guy:on_interaction()
  game:start_dialog("out.e2.bad_guy_welcome", function()
    game:remove_money(5)
  end)
end

function mine_keeper:on_interaction()
  local mine_opening_quest = game:get_value("mine_opening_quest")

  if not game:get_value("mine_opened") and mine_opening_quest == 5 then
    game:start_dialog("out.e2.mine_keeper.granted")
    self:walk_to(mine_keeper_target)
    game:set_value("mine_opened", true)
  elseif mine_opening_quest == 4 then
    game:start_dialog("out.e2.mine_keeper.someone_locked")
  elseif mine_opening_quest == 2 or mine_opening_quest == 3 then
    game:start_dialog("out.e2.mine_keeper.forbidden")
    game:set_value("mine_opening_quest", 3)
  else
    game:start_dialog("out.e2.mine_keeper.silent")
  end
end

function mine_keeper:walk_to(target)
  local movement = sol.movement.create("target")
  movement:set_target(target)
  movement:set_speed(35)

  function movement:on_finished()
    mine_keeper:get_sprite():set_direction(3)
	end

  movement:start(self)
end

function bottle_vendor:on_interaction()

  if game:has_item("bottle_1") then
    game:start_dialog("bottle_vendor.done")
  else
    game:start_dialog("bottle_vendor.offer", function(answer)
      if answer == 4 then  -- No.
        game:start_dialog("bottle_vendor.no")
      else  -- Yes.
        if game:get_money() < 100 then
          game:start_dialog("not_enough_money")
        else
          game:start_dialog("bottle_vendor.yes", function()
            game:remove_money(100)
            hero:start_treasure("bottle_1")
          end)
        end
      end
    end)
  end
end

function northeast_woman:on_interaction()
  game:start_dialog("out.e2.northeast_woman")
  if game:get_value("mine_opening_quest") == nil then
    game:set_value("mine_opening_quest", 1)
  else
    print(game:get_value("mine_opening_quest"))
  end
end

function northwest_man:on_interaction()
  local mine_opening_quest = game:get_value("mine_opening_quest")

  if mine_opening_quest == nil then
    game:start_dialog("out.e2.northwest_man.go_away")
  else
    game:start_dialog("out.e2.northwest_man.find_the_miner")
    if mine_opening_quest == 1 then
      game:set_value("mine_opening_quest", 2)
    end
  end
end