local map = ...
local game = map:get_game()

local fighting_boss = false
local music_id = map:get_music()

function map:on_started()

  if game:has_item("bombs_counter") then
    final_npc:remove()
  end

  if boss ~= nil then
    boss:set_enabled(false)
  end
  map:set_doors_open("boss_door", true)
end

function start_boss_sensor:on_activated()

  if boss ~= nil and not fighting_boss then
    hero:freeze()
    map:close_doors("boss_door")
    sol.audio.stop_music()
    sol.timer.start(1000, function()
      boss:set_enabled(true)
      hero:unfreeze()
      sol.audio.play_music("boss")
      fighting_boss = true
    end)
  end
end

function map:on_obtained_treasure(item, variant, savegame_variable)

  if item:get_name() == "heart_container" then
    sol.audio.play_music(music_id)
    map:open_doors("boss_door")
  end
end

function final_npc:on_interaction()
  if not game:has_item("bombs_counter") then
    game:start_dialog("dungeons.2.final_npc", function()
      hero:start_treasure("bombs_counter")

      local bombs_counter = game:get_item("bombs_counter")
      bombs_counter:add_amount(bombs_counter:get_max_amount())
    end)
  end
end