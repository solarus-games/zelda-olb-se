local map = ...
local game = map:get_game()

local fighting_boss = false
local music_id = map:get_music()

function map:on_started()

  if boss ~= nil then
    boss:set_enabled(false)
  end
  map:set_doors_open("boss_door", true)
end

function start_boss_sensor:on_activated()
  if boss ~= nil and not fighting_boss then
    hero:freeze()
    map:close_doors("boss_door")
    sol.audio.stop_music()
    sol.timer.start(1000, function()
      boss:set_enabled(true)
      hero:unfreeze()
      sol.audio.play_music("boss")
      fighting_boss = true
    end)
  end
end

function map:on_obtained_treasure(item, variant, savegame_variable)

  if item:get_name() == "heart_container" then

    sol.audio.play_music(music_id)
    map:open_doors("boss_door")

  elseif item:get_name() == "graal" then

    game:set_dungeon_finished()
    hero:set_direction(3)
    hero:start_victory(function()
      game:start_dialog("dungeon_finished_save", function(answer)
        sol.audio.play_sound("danger")
        if answer == 3 then
          game:save()
        end
        hero:unfreeze()
      end)
    end)

  end
end