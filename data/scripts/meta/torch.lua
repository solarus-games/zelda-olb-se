-- Initializes torch behavior.

-- Reset torches info if the world changes.
local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_world_changed", function(game, previous_world, new_world)
  game.lit_torches_by_map = nil
end)
